(ns jd2ds.main
  (:require [jd2ds.core :as core]
            [clojure.string :as string]
            [clojure.tools.cli :as cli]
            [clojure.set :as set])
  (:gen-class))

;;-----------------------------------------------------------------------------
;; Helper Utils
;;
(defn missing-required? [opts reqs]
  (not-every? opts reqs))

(defn required-error-msg [opts reqs]
  (str "The following options are required:\n\n   "
       (string/join "\n   " (map name (set/difference reqs opts)))
       "\n"))

(defn error-msg [errors]
  (str "The following errors occured while parsing your command:\n\n"
       (string/join \newline errors)
       "\n"))

(defn exit [status & messages]
  (println (string/join \newline messages))
  (System/exit status))


;;-----------------------------------------------------------------------------
;; Main CLI Program
;;
(def cli-options
  [["-o" "--out PATH" "Path to write resulting docset archive"
    :parse-fn #(clojure.java.io/file %)
    :validate [#(or (not (.exists %))
                         (.isFile %)) "Path must not be a directory"
               #(-> %
                    .getAbsoluteFile
                    .getParentFile
                    .canWrite) "docset path must be writable"]]
   ["-n" "--docset-name DOCSET_NAME" "DocSet Name" :id :docset]
   ["-h" "--help"]])

(def required-opts #{:docset :out})

(defn usage [options-summary]
  (->> ["Javadoc to DocSet"
        ""
        "Usage: jd2ds [options] javadoc"
        ""
        "Options:"
        options-summary]
       (clojure.string/join \newline)))

(defn path-exists? [path] (.exists (clojure.java.io/file path)))

(defn -main [& args]
  (let [{:keys [options errors summary arguments]} (cli/parse-opts args cli-options)]
    (cond
      (:help options) (exit 0 (usage summary))

      ;; Exit on errors during option validation/parsing
      errors (exit 1
                   (error-msg errors)
                   (usage summary))

      ;; Exit when required arguments are not present
      (missing-required? options required-opts) (exit 1
                                                      (required-error-msg options required-opts)
                                                      (usage summary))

      ;; Exit when path to javadoc is not provided
      (empty? arguments) (exit 1
                               "javadoc must be provided\n"
                               (usage summary))

      ;; Exit when provided javadoc does not exist
      (not
        (path-exists? (first arguments))) (exit 1
                                             "javadoc must exist\n"
                                             (usage summary))

      ;; Create the DocSet
      :default
      (let [options (assoc options :javadoc (first arguments))]
        (core/create-docset! options)))))
