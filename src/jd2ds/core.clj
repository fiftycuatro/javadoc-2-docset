(ns jd2ds.core
  (:require [clojure.java.io :as io]
            [net.cgrand.enlive-html :as html]
            [clojure.java.jdbc :as jdbc])
  (:import [java.util.zip ZipFile GZIPOutputStream]
           [java.io FileOutputStream BufferedOutputStream]
           [org.apache.commons.compress.archivers.tar TarArchiveOutputStream TarArchiveEntry]))

;;-----------------------------------------------------------------------------
;; Utils I wish were in the stdlib

(def empty-fileattrs (into-array java.nio.file.attribute.FileAttribute []))
(defn create-tmp-dir!
  [{:keys [prefix] :or [prefix "tmp"] :as opts}]
  (let [temp-path (java.nio.file.Files/createTempDirectory prefix empty-fileattrs)
        temp-file (.toFile temp-path)]
    (.deleteOnExit temp-file)
    temp-file))

;;-----------------------------------------------------------------------------
;; Archive Utils

(defn extract-zip!
  "Takes the src zip file and extracts it to dst location"
  [src, dst]
  (with-open [zip (ZipFile. src)]
    (let [entries (enumeration-seq (.entries zip))
          dest-path #(io/file dst (str %))]
      (doseq [entry entries :when (not (.isDirectory entry))
              :let [f (dest-path entry)]]
        (io/make-parents f)
        (io/copy (.getInputStream zip entry) f))))
  dst)

(defn write-tar-entries!
  ^{:doc "Recursively writes src directory TarArchiveEntries to out"
    :private true}
  [in out path]
  (let [entry-path (clojure.string/join java.io.File/separator [path (.getName in)])]
    (if (.isDirectory in)
      (doseq [f (.listFiles in)] (write-tar-entries! f out entry-path))
      (do
        (.putArchiveEntry out (TarArchiveEntry. in entry-path))
        (io/copy in out)
        (.closeArchiveEntry out)))))

(defn compress-tar-gz!
  "Compresses the source file/path to out"
  [src ^java.io.File dst]
  (with-open [out (-> (FileOutputStream. dst)
                      BufferedOutputStream.
                      GZIPOutputStream.
                      TarArchiveOutputStream.)]
    (.setLongFileMode out (TarArchiveOutputStream/LONGFILE_POSIX))
    (write-tar-entries! src out ".")
    dst))

;; -----------------------------------------------------------------------------
;; plist Utils

(defn prop->xml
  [[key val]]
  (let [val (cond
              (instance? Boolean val) (if val "<true/>" "<false />")
              (keyword? val) (str "<string>" (name val) "</string>")
              :default (str "<string>" val "</string>"))]
    (str "<key>" (name key) "</key>\n" val)))

(defn plist
  [props]
  (let [header "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<plist version=\"1.0\">\n<dict>\n"
        footer "</dict>\n</plist>\n"]
    (str header (clojure.string/join "\n" (map prop->xml props)) footer)))

(defn write-docset-plist!
  [f name index]
  (let [props
        [[:CFBundleIdentifier name]
         [:CFBundleName name]
         [:DocSetPlatformFamily "javadoc"]
         [:isDashDocSet true]
         [:dashIndexFilePath index]
         [:DashDocSetFamily "java"]]]
    (spit f (plist props))))

;;------------------------------------------------------------------------------
;; Database

(defn create-db
  [db-file]
  {:classname   "org.sqlite.JDBC"
   :subprotocol "sqlite"
   :subname     (str db-file)
   })

(defn init-db!
  [db]
  (let [table-ddl (jdbc/create-table-ddl :searchIndex
                                         [[:id   :integer "PRIMARY KEY"]
                                          [:name :text]
                                          [:type :text]
                                          [:path :text]])]
    (try
      (jdbc/db-do-commands db table-ddl)
      (catch Exception e
        (println e)))))

(defn add-entries-db!
  [db entries]
  (doseq [entry entries]
    (jdbc/insert! db :searchIndex entry)))

;;-----------------------------------------------------------------------------
;; Javadoc to DocSet

(defn all-indices
  "Returns a list of files to javadoc index files from dst"
  [documents]
  (let [single-file (io/file documents "index-all.html")
        split-path (io/file documents "index-files")
        is-index? #(re-find #"^index-\d+\.html$" (.getName %))
        is-dir?    #(not (.isDirectory %))
        split-files #(filter (every-pred is-index? is-dir?) (.listFiles %))]
    (cond
      (.exists single-file) [single-file]
      (.isDirectory split-path) (split-files split-path)
      :default [])))

(def type-mapping
  [
   ; 3-Tuple
   ; Type        Text Contents                                    dt class
   "Class"       ["Class in" "- class"]                          ["class"]
   "Method"      ["Static method in" "Method in"]                ["method"]
   "Field"       ["Static variable in" "Field in" "Variable in"] ["field"]
   "Constructor" ["Constructor"]                                 ["constructor"]
   "Interface"   ["Interface in" "- interface"]                  ["interface"]
   "Exception"   ["Exception in" "- exception"]                  ["exception"]
   "Error"       ["Error in" "- error"]                          ["error"]
   "Enum"        ["Enum in" "- enum"]                            ["enum"]
   "Trait"       ["Trait in"]                                    []
   "Notation"    ["Annotation Type"]                             ["annotation"]
   "Package"     ["package"]                                     ["package"]])

(defn dt->type
  "Maps a dt node into a DocSet type"
  [dt]
  (let [text (html/text dt)
        dt-class (get-in dt [:attrs :href] "")
        is-match? (fn [[type text-s class-s]]
                    (or (some #(.contains text %) text-s)
                        (some #(.contains dt-class %) class-s)))]
    (or
      (first (first (filter is-match? (partition 3 type-mapping))))
      "")))

(defn dt->entry
  "Converts a dt node into an entry"
  [dt]
  (let [anchor-el (first (html/select dt [:a]))
        name (html/text anchor-el)
        path (get-in anchor-el [:attrs :href])
        type (dt->type dt)]
    {:name name :type type :path path}))

(defn index->entries
  "Provides sequence of docset entries from javadoc index"
  [index]
  (let [doc (html/html-resource index)
        datatypes (html/select doc [:dt])]
    (map dt->entry datatypes)))
  
(defn all-entries
  "Returns a sequence of docset entries"
  ([indices] (all-entries nil indices))
  ([entries indices]
   (cond
     (not (empty? entries)) (lazy-seq (cons (first entries) (all-entries (rest entries) indices)))
     (not (empty? indices)) (all-entries (index->entries (first indices)) (rest indices))
     :default nil
    )))

(defn create-docset!
  [{:keys [javadoc docset out]}]

  ;; Construct DocSet in temp location that gets deleted on exit
  (let [tmp-dir (create-tmp-dir! {:prefix "jd2ds"})
        root (io/file tmp-dir (str docset ".docset"))
        contents (io/file root "Contents")
        resources (io/file contents "Resources")
        documents (io/file resources "Documents")
        db (create-db (io/file resources (str "docSet.dsidx")))]

    ;; Initialize directories and DB
    (doseq [d [root contents resources documents]] (.mkdirs d))
    (init-db! db)

    ;; Create DocSet
    (->> (extract-zip! javadoc documents)
         all-indices
         all-entries
         (add-entries-db! db))
    (write-docset-plist! (io/file contents "Info.plist") name "index.html")

    ;; Archive DocSet
    (compress-tar-gz! root out)))


