(defproject jd2ds "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.8.0"]
                 [enlive "1.1.6"]
                 [org.clojure/java.jdbc "0.7.5"]
                 [org.xerial/sqlite-jdbc "3.21.0.1"]
                 [org.apache.commons/commons-compress "1.15"]
                 [org.clojure/tools.cli "0.3.5"]]

  :profiles {:uberjar {:aot        :all
                       :main       jd2ds.main}})
